# Autor: Maria Andreina Loriente

class { 'memcached': }

class { 'apache': }

class { 'mysql': }

class { 'php': 
	version => '5.5',
}


file { "/web/index.php":

	replace => true,
	ensure => present,
	source => "puppet:///pages/index.php",

}

file { "/web/info.php":

	replace => true,
	ensure => present,
	source => "puppet:///pages/info.php",

}

mysql::grant { 'mympwar':

	mysql_privileges => 'ALL',
	mysql_password => 'password_mpwar',
	mysql_db => 'mympwar',
	mysql_user => 'mympwar',

}